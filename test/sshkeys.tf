resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCbN9ns0eSNz+E2ST+Nr6FCb+vDuFAOHjsnBqVaPiAxiU7l0YhkNQKVIFmMrqIfgO0Xb1JUjCGdhw5C/eJiG74b+KW7m8WSL2kh+hNYZaitjfpf0EX3qwEEMTruru8Fqya1eTYMv9dvq7ZsKzFKL/iCKVrBHtREYwu96YaVoSd6bNt/cU22WatKzwX5fVQ9FZT1FOtn4dtddpEjRd1lD/OmF+zPWxv9+KWzlX3xv0TXbWdpevTYdz0aaNFaeC7RqowSWiyrIz+O6p2me2rdsTdeOmRC0xY/MSYTsSK4oVDc83W+N9hN9wwuBfCnJN+iHeCrUsPZZ15IU/3KMzf360MB vagrant@inframaster"
}