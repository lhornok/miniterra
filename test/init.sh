#!/usr/bin/env bash

terraform init \
        -backend-config=address=https://gitlab.com/api/v4/projects/23748169/terraform/state/test \
        -backend-config=lock_address=https://gitlab.com/api/v4/projects/23748169/terraform/state/test/lock \
        -backend-config=unlock_address=https://gitlab.com/api/v4/projects/23748169/terraform/state/test/lock \
        -backend-config=username=lhornok \
        -backend-config=password=uEUqsSrXHGuPsZVZ2Hdy \
        -backend-config=lock_method=POST \
        -backend-config=unlock_method=DELETE \
        -backend-config=retry_wait_min=5
