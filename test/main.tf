############################################
# PROVIDER FOR TERRAFORM                   #
# OUTPUT TO ANSIBLE HOST FILE              #
############################################

provider "aws" {
  profile = "default"
  region  = var.region
}