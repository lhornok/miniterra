resource "local_file" "AnsibleInventory" {
    content = templatefile("inventory.tmpl",
    {
        kubemaster_dns = aws_instance.kubemaster.public_dns
    }
    )
  filename = "inventory"
}

resource "local_file" "AnsibleIP" {
    content = templatefile("ip.tmpl",
    {
        kubemaster_ip = aws_instance.kubemaster.private_ip
        kubemaster_public_ip = aws_instance.kubemaster.public_ip
    }
    )
  filename = "ip"
}

resource "local_file" "AWSInstanceID" {
    content = templatefile("instance_id.tmpl",
    {
        kubemaster_instance_id = aws_instance.kubemaster.id
    }
    )
  filename = "instance_id"
}