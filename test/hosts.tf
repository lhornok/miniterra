#################################################################################
# EC2 INSTANCE                                                                  #
# Ubuntu Server 18.04 LTS (HVM), SSD Volume Type                                #
################################################################################

resource "aws_instance" "kubemaster" {
  ami                         = "ami-089cc16f7f08c4457"
  instance_type               = "t2.medium"
  key_name                    = "deployer-key"
  subnet_id                   = aws_subnet.private_subnet_a.id
  vpc_security_group_ids      = [aws_security_group.kubemaster.id, aws_security_group.sg_ssh.id,aws_security_group.kube_api.id]
  associate_public_ip_address = true
  source_dest_check           = false
}