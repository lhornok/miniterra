#!/bin/bash

KUBEDEFAULTTOKEN=$( kubectl get secrets | egrep  -Eo 'default-token-[a-zA-Z0-9.]+' )
KUBECACERTIFICATE=$( kubectl get secret "$KUBEDEFAULTTOKEN" -o jsonpath="{['data']['ca\.crt']}" | base64 --decode )
kubectl apply -f gitlab-admin-service-account.yaml
KUBESECRETDESCRIBE=$( kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}') | grep -E '^token' )
echo "$KUBECACERTIFICATE"
echo "$KUBESECRETDESCRIBE"